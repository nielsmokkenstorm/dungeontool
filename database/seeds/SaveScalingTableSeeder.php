<?php

use Illuminate\Database\Seeder;
use DungeonTool\Models\Core\SaveScaling;

class SaveScalingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(DungeonTool\Models\Core\SaveScaling::class, 2)->create();
    }
}
