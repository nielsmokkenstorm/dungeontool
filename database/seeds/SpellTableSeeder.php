<?php

use Illuminate\Database\Seeder;
use DungeonTool\Models\Magic\Spell;
use DungeonTool\Models\Magic\SpellSchool;

class SpellTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(DungeonTool\Models\Magic\Spell::class, 50)->create();
    }
}
