<?php

use Illuminate\Database\Seeder;
use DungeonTool\Models\Core\CharacterClass;

class CharacterClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(DungeonTool\Models\Core\CharacterClass::class, 10)->create();
    }
}
