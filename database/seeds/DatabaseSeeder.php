<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UserTableSeeder::class);
        $this->call(SpellSchoolTableSeeder::class);
    	$this->call(SpellTableSeeder::class);
        
        $this->call(SaveScalingTableSeeder::class);
        $this->call(BaseAttackBonusScalingTableSeeder::class);
        $this->call(CharacterClassTableSeeder::class);
    }
}
