<?php

use Illuminate\Database\Seeder;
use DungeonTool\Models\Core\BaseAttackBonusScaling;

class BaseAttackBonusScalingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(DungeonTool\Models\Core\BaseAttackBonusScaling::class, 3)->create();
    }
}
