<?php

use Illuminate\Database\Seeder;
use DungeonTool\Models\Magic\SpellSchool;

class SpellSchoolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(SpellSchool::class, 10)->create();
    }
}
