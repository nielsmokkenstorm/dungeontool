<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(DungeonTool\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'              => $faker->name,
        'email'             => $faker->unique()->safeEmail,
        'password'          => $password ?: $password = bcrypt('secret'),
        'remember_token'    => str_random(10),
    ];
});

$factory->define(DungeonTool\Models\Magic\SpellSchool::class, function (Faker\Generator $faker) {

    return [
        'name'              => $faker->name,
    ];
});

$factory->define(DungeonTool\Models\Magic\Spell::class, function (Faker\Generator $faker) {

    return [
        'name'              => $faker->name,
        'description'       => $faker->paragraph,
        'spell_school_id'   => \DungeonTool\Models\Magic\SpellSchool::all()->random()->id,
    ];
});

$factory->define(DungeonTool\Models\Core\BaseAttackBonusScaling::class, function (Faker\Generator $faker) {

    return [
        'name'              =>  $faker->randomElement(array ('Full','Medium','Half')),
        'value'             =>  $faker->randomFloat(2,0,1),
    ];
});

$factory->define(DungeonTool\Models\Core\SaveScaling::class, function (Faker\Generator $faker) {

    return [
        'name'              =>  $faker->randomElement(array ('Good', 'Bad')),
        'value'             =>  $faker->randomFloat(2,0,1),
    ];
});

$factory->define(DungeonTool\Models\Core\CharacterClass::class, function (Faker\Generator $faker) {

    return [
        'name'              => $faker->name,
        'base_attack_bonus'      => \DungeonTool\Models\Core\BaseAttackBonusScaling::all()->random()->id,
        'fortitude'         => \DungeonTool\Models\Core\SaveScaling::all()->random()->id,
        'will'              => \DungeonTool\Models\Core\SaveScaling::all()->random()->id,
        'reflex'            => \DungeonTool\Models\Core\SaveScaling::all()->random()->id,
    ];
});