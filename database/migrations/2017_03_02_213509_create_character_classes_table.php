<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->integer('base_attack_bonus')->unsigned();
            $table->foreign('base_attack_bonus')->references('id')->on('base_attack_bonus_scalings');

            $table->integer('fortitude')->unsigned();
            $table->foreign('fortitude')->references('id')->on('save_scalings');

            $table->integer('will')->unsigned();
            $table->foreign('will')->references('id')->on('save_scalings');

            $table->integer('reflex')->unsigned();
            $table->foreign('reflex')->references('id')->on('save_scalings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_classes');
    }
}
