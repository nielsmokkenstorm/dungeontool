<?php

namespace DungeonTool\Models\Core;

use DungeonTool\Models\Base\BaseModel;

class SaveScaling extends BaseModel
{
	// we're not interested in timestamps
	public $timestamps = false;
}
