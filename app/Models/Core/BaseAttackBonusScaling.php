<?php

namespace DungeonTool\Models\Core;

use Illuminate\Database\Eloquent\Model;

class BaseAttackBonusScaling extends Model
{
	// we're not interested in timestamps
	public $timestamps = false;
}
