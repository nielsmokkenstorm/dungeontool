<?php

namespace DungeonTool\Models\Core;

use DungeonTool\Models\Base\BaseModel;

class CharacterClass extends BaseModel
{
    /**
     * Get the base attack bonus asociated with this class
     */
    public function baseAttackBonus()
    {
        return $this->hasOne('DungeonTool\Models\Core\BaseAttackBonusScaling', 'id', 'base_attack_bonus');
    }

    /**
     * Get the fortitude save asociated with this character class
     */
    public function fortitudeSave()
    {
        return $this->hasOne('DungeonTool\Models\Core\SaveScaling', 'id', 'fortitude');
    }

    /**
     * Get the reflex save asociated with this character class
     */
    public function reflexSave()
    {
        return $this->hasOne('DungeonTool\Models\Core\SaveScaling', 'id', 'reflex');
    }

    /**
     * Get the will save asociated with this character class
     */
    public function willSave()
    {
        return $this->hasOne('DungeonTool\Models\Core\SaveScaling', 'id', 'will');
    }

    /**
     * Get the list overview uri 
     */
    public function listUri()
    {
        return route('character-class-list');
    }

    /**
     * Get the detail overview uri 
     */
    public function detailUri()
    {
        return route('character-class-detail', ['id' => $this->id]);
    }
}
