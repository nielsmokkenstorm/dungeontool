<?php

namespace DungeonTool\Models\Magic;

use DungeonTool\Models\Base\BaseModel;

class SpellSchool extends BaseModel
{
    /**
     * Get the spells for the school.
     */
	public function spells()
    {
        return $this->hasMany('DungeonTool\Models\Magic\Spell');
    }
}
