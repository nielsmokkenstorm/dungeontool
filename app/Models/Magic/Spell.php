<?php

namespace DungeonTool\Models\Magic;

use DungeonTool\Models\Base\BaseModel;

class Spell extends BaseModel
{
	// we're not interested in timestamps
	public $timestamps = false;

	/**
     * Get the school record associated with the spell.
     */
    public function spellSchool()
    {
        return $this->belongsTo('DungeonTool\Models\Magic\SpellSchool');
    }

    public function listUri()
    {
        return route('spell-list');
    }

    public function detailUri()
    {
    	return route('spell-detail', ['id' => $this->id]);
    }
}
