<?php

namespace DungeonTool\Models\Base;

use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
	// we're not interested in timestamps, unless we are
	public $timestamps = false;
}
