<?php

namespace DungeonTool\Http\Controllers;

use Illuminate\Http\Request;
use DungeonTool\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {
        $spells = Spell::paginate(10);
        $links  = $spells->links();

        return view('spell.index', ['spells' => $spells, 'pagination' => $links]);
    }
}
