<?php

namespace DungeonTool\Http\Controllers;

use DungeonTool\Models\Magic\Spell;
use Illuminate\Http\Request;
use DungeonTool\Http\Controllers\Controller;

class SpellController extends Controller
{
    /**
     * Show a list of all of the application's spells.
     *
     * @return Response
     */
    public function index()
    {
        $spells = Spell::paginate(10);
        $links  = $spells->links();

        return view('spell.index', ['spells' => $spells, 'pagination' => $links]);
    }

    /**
     * Show a detail view for this user
     *
     * @return Response
     */
    public function detail($id)
    {
        $spell  = Spell::find($id);

        return view('spell.detail', ['spell' => $spell]);
    }
}
