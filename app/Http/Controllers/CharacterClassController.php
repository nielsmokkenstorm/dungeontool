<?php

namespace DungeonTool\Http\Controllers;

use DungeonTool\Models\Core\CharacterClass;
use Illuminate\Http\Request;
use DungeonTool\Http\Controllers\Controller;

class CharacterClassController extends Controller
{
    /**
     * Show a list of all of the application's spells.
     *
     * @return Response
     */
    public function index()
    {
        $characterclasses = CharacterClass::paginate(10);
        $links  = $characterclasses->links();

        return view('characterclass.index', ['characterclasses' => $characterclasses, 'pagination' => $links]);
    }

    /**
     * Show a detail view for this user
     *
     * @return Response
     */
    public function detail($id)
    {
        $characterclass  = CharacterClass::find($id);
        $characterclass->load('baseAttackBonus', 'fortitudeSave', 'willSave', 'reflexSave');

        return view('characterclass.detail', ['characterclass' => $characterclass]);
    }
}
