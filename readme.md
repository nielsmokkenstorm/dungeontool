# DungeonTool
Add some description here...

## Development environment
Run both `composer install` and `npm install` from the project root to install the project dependencies.
To migrate and seed the database run `php artisan migrate --seed`.

To run the project, make sure you have [Docker](https://www.docker.com/) installed.
This project uses Laradock to run locally, so from the `laradock` directory, start the nginx and mysql containers 
with `docker-compose up -d nginx mysql`.
You can log into the docker workspace container with `docker-compose exec workspace bash`.

## License

This is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
