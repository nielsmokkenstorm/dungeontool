@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Spells</li>
                    </ol>

                    <h1> Spells </h1>

                </div>

                <div class="panel-body">

                    @forelse ($spells as $spell)

                        @if ($loop->first)
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                    <th>School</th>
                                </tr>
                        @endif
                        <tr data-href="{{ $spell->detailUri() }}">
                            <td>{{ $spell->name }}</td>
                            <td>{{ $spell->spellschool->name}} </td>
                        </tr>

                        @if ($loop->last)
                            </table>
                        @endif

                    @empty
                        <p>No spells</p>
                    @endforelse

                    <div class="text-center">
                        {{$pagination}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')