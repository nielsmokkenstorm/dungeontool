@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default">
                
                <div class="panel-heading">
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li><a href="{{$spell->listUri()}}">Spells</a></li>
                        <li class="active">{{$spell->name}}</li>
                    </ol>
                    <h1> {{$spell->name}} </h1>
                </div>

                <div class="panel-body">
                    {{$spell->description}}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection('content')