@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Character Classes</li>
                    </ol>

                    <h1> Classes </h1>

                </div>

                <div class="panel-body">

                	@forelse ($characterclasses as $class)

                        @if ($loop->first)
                            <table class="table">
                                <tr>
                                    <th>Name</th>
                                </tr>
                        @endif
				        <tr data-href="{{ $class->detailUri() }}">
                            <td>{{ $class->name }}</td>
                        </tr>

                        @if ($loop->last)
                            </table>
                        @endif

					@empty
					    <p>No classes</p>
					@endforelse

                    <div class="text-center">
                        {{$pagination}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection('content')