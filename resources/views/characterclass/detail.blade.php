@extends('layouts.detail')

@section('title')
    {{$characterclass->name}}
@endsection('title')

@section('description')
    {{$characterclass->description}}
@endsection('description')

@section('breadcrumbs')

    <li>
        <a href="/">Home</a>
    </li>
    <li>
        <a href="{{$characterclass->listUri()}}">Classes</a>
    </li>
    <li class="active">
        {{$characterclass->name}}
    </li>

@endsection('breadcrumbs')

@section('table')

    <table class="table">
        <thead>
            <tr>
                <th>
                    Level
                </th>
                <th>
                    BaB
                </th>
                <th>
                    Fortitude
                </th>
                <th>
                    Will
                </th>
                <th>
                    Reflex
                </th>
            </tr>
        </thead>
        <tbody>
            @for ($i = 1; $i <= 20; $i++)
                <tr>
                    <td>
                        {{ $i }}
                    </td>
                    <td>
                        +{{ floor($i * $characterclass->baseAttackBonus->value)}}
                    </td>
                    <td>
                        +{{ floor($i * $characterclass->fortitudeSave->value)}}
                    </td>
                    <td>
                        +{{ floor($i * $characterclass->willSave->value)}}
                    </td>
                    <td>
                        +{{ floor($i * $characterclass->reflexSave->value)}}
                    </td>
                </tr>
            @endfor
        </tbody>
    </table>

@endsection('table')